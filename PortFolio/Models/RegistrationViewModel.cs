﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PortFolio.Models
{
    public class RegistrationViewModel
    {
        public RegistrationViewModel()
        {
            this.RegistrationList = new List<RegistrationViewModel>();
            this.CountryList = new List<SelectListItem>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public byte[] Image { get; set; }
        public string ImageName { get; set; }

        public IList<RegistrationViewModel> RegistrationList { get; set; }
        public IList<SelectListItem> CountryList { get; set; }
    }
}