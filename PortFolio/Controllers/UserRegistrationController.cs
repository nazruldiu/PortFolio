﻿using PortFolio.BDEntity;
using PortFolio.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PortFolio.Controllers
{
    public class UserRegistrationController : Controller
    {
        private PortFolioEntities db = new PortFolioEntities();
        // GET: UserRegistration
        public ActionResult Index()
        {
            RegistrationViewModel model = new RegistrationViewModel();
        
            var list = db.RegistrationTbls.ToList();
            List<RegistrationViewModel> TempList = new List<RegistrationViewModel>();

            foreach (var item in list)
            {
                var obj = new RegistrationViewModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    MobileNo = item.Mobile,
                    BirthDate = item.BirthDate,
                    Email = item.Email
                };
                TempList.Add(obj);
            }
            model.RegistrationList = TempList;
            return View(model);
        }

        // GET: UserRegistration/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: UserRegistration/Create
        public ActionResult Create()
        {
            RegistrationViewModel model = new RegistrationViewModel();

            dynamic countryList = db.CountryTbls.ToList();

            var list = new List<SelectListItem>();
            foreach (var item in countryList)
            {
                list.Add(new SelectListItem()
                {
                    Text = item.Name,
                    Value = item.Id.ToString()
                });
            }
            model.CountryList = list;
            return View(model);
        }

        // POST: UserRegistration/Create
        [HttpPost]
        public ActionResult Create(RegistrationViewModel model, HttpPostedFileBase file)
        {
            try
            {
                RegistrationTbl tbl = new RegistrationTbl();
                tbl.Name = model.Name;
                tbl.CountryId = model.CountryId;
                tbl.BirthDate = model.BirthDate;
                tbl.Email = model.Email;
                tbl.Mobile = model.MobileNo;

                tbl.ImageName = file.ContentType;

                Int32 length = file.ContentLength;
                byte[] tempImage = new byte[length];
                file.InputStream.Read(tempImage, 0, length);
                tbl.Image = tempImage;

                db.RegistrationTbls.Add(tbl);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: UserRegistration/Edit/5
        public ActionResult Edit(int id)
        {
            RegistrationViewModel model = new RegistrationViewModel();
            var entity = db.RegistrationTbls.Find(id);

            model.Name = entity.Name;
            model.CountryId = entity.CountryId;
            model.Email = entity.Email;
            model.MobileNo = entity.Mobile;
            model.BirthDate = entity.BirthDate;
            model.Image = entity.Image;
            return View(model);
        }

        // POST: UserRegistration/Edit/5
        [HttpPost]
        public ActionResult Edit(RegistrationViewModel model, HttpPostedFileBase file)
        {
            try
            {
                RegistrationTbl tbl = new RegistrationTbl();
                tbl.Id = model.Id;
                tbl.Name = model.Name;
                tbl.CountryId = model.CountryId;
                tbl.BirthDate = model.BirthDate;
                tbl.Email = model.Email;
                tbl.Mobile = model.MobileNo;
                tbl.ImageName = model.ImageName;
                tbl.Image = model.Image;
                if (file != null)
                {
                    tbl.ImageName = file.ContentType;

                    Int32 length = file.ContentLength;
                    byte[] tempImage = new byte[length];
                    file.InputStream.Read(tempImage, 0, length);
                    tbl.Image = tempImage;
                }
                db.Entry(tbl).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View(model);
            }
        }

        // GET: UserRegistration/Delete/5
        public ActionResult Delete(int id)
        {
            RegistrationTbl registrationTbl = db.RegistrationTbls.Find(id);
            db.RegistrationTbls.Remove(registrationTbl);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // POST: UserRegistration/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
