﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PortFolio.BDEntity;

namespace PortFolio.Controllers
{
    public class RegistrationController : Controller
    {
        private PortFolioEntities db = new PortFolioEntities();

        // GET: Registration
        public ActionResult Index()
        {
            return View(db.RegistrationTbls.ToList());
        }

        // GET: Registration/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RegistrationTbl registrationTbl = db.RegistrationTbls.Find(id);
            if (registrationTbl == null)
            {
                return HttpNotFound();
            }
            return View(registrationTbl);
        }

        // GET: Registration/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Registration/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,CountryId,BirthDate,Email,Mobile,Image,ImageName")] RegistrationTbl registrationTbl)
        {
            if (ModelState.IsValid)
            {
                db.RegistrationTbls.Add(registrationTbl);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(registrationTbl);
        }

        // GET: Registration/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RegistrationTbl registrationTbl = db.RegistrationTbls.Find(id);
            if (registrationTbl == null)
            {
                return HttpNotFound();
            }
            return View(registrationTbl);
        }

        // POST: Registration/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,CountryId,BirthDate,Email,Mobile,Image,ImageName")] RegistrationTbl registrationTbl)
        {
            if (ModelState.IsValid)
            {
                db.Entry(registrationTbl).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(registrationTbl);
        }

        // GET: Registration/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RegistrationTbl registrationTbl = db.RegistrationTbls.Find(id);
            if (registrationTbl == null)
            {
                return HttpNotFound();
            }
            return View(registrationTbl);
        }

        // POST: Registration/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RegistrationTbl registrationTbl = db.RegistrationTbls.Find(id);
            db.RegistrationTbls.Remove(registrationTbl);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
